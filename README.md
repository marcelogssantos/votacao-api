# Votacao
Repositorio: https://github.com/MarceloGiovani/votacao

Desenvolvida Java 17, usando os frameworks: Spring Boot na versão 2.6.6, Hibernate e o Maven. 
Esta API REST faz uso de endpoints com os seguintes verbos GET, POST e PUT, onde permite a busca, criaçãa e atualização de dados. Ainda usa o banco de dados postgres 14.

# Objetivo da aplicação
Permitir que uma pauta de um determinadi assunto possa ser votado por associados.
Para isso é possível:
    Cadastrar, atualizar e consultar Associados
    Cadastrar e atualizar e consultar Pauta
    Cadastrar e consultar Votacao
    Registrar(cadastrar) Voto

# Levantando a aplicação 
Uma vez clonado o repositorio, há anecessidade de ser ter uma coneção local do banco de dados postgres13 ou 14 usando a porta 5433 ou pode-se fazer a alteração no "application.properties" para apontar para a conexão desejada.

# Endpoint
Documentaçao detalhada acesso local: http://localhost:8080/api/swagger-ui/index.html

    Associado
        GET:  api/v1/associados/{id} (Busca por ID)
              api/v1/associados/lista (Busta todos ou passando por Json o Titulo)
        POST: api/v1/associados (Cria um Associado)
        PUT:  api/v1/associados (Altera dados do associado)

    Pata
        GET:  api/v1/pautas/{id} (Busca por ID)
              api/v1/pautas/lista (Busta todos ou passando por Json o nome)
        POST: api/v1/pautas (Cria um Associado)
        PUT:  api/v1/pautas (Altera dados do associado)

    Votacao
        GET:  api/v1/votacoes//status/{id} (Busca a situacao da votacao ID)
              api/v1/votacoes/lista (Busta todas as votacoes)
        POST: api/v1/votacoes/abrirVotacao (Cria uma votação esperando ser votada)
        PUT:  api/v1/votacoes/encerrarVotacao/{tipoEncerramento} (Encerra a votação de forma forçada)
    Voto
        POST: api/v1/votos (Registra o voto do associado)
