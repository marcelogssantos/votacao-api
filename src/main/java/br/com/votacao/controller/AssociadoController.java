package br.com.votacao.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.votacao.error.ResponseMsg;
import br.com.votacao.model.Associado;
import br.com.votacao.model.dto.AssociadoDto;
import br.com.votacao.repository.AssociadoRepository;
import br.com.votacao.service.AssociadoService;


@RestController
@RequestMapping("/v1/associados")
public class AssociadoController {
	
	@Autowired
	private AssociadoRepository repository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private AssociadoService associadoService;
	
	@GetMapping("/{id}")
	@ResponseBody
	public AssociadoDto find(@PathVariable Long id) {
		return modelMapper.map(repository.findById(id).get(), AssociadoDto.class);
	}
	
	@GetMapping("/lista")
	@ResponseBody
	public Page<AssociadoDto> findAssociados(@RequestParam(required = false) String nome, 
			@PageableDefault(sort = "id", direction = Direction.DESC, page = 0, size = 10) Pageable pageable) {
		Page<AssociadoDto> associasdDto = null;
		if (nome == null) {
			associasdDto = repository.findAll(pageable).map(a -> modelMapper.map(a, AssociadoDto.class));
		} else {
			associasdDto = repository.findByNome(nome, pageable).map(a -> modelMapper.map(a, AssociadoDto.class));
		}
		
		return associasdDto;
	}
	
	@SuppressWarnings("rawtypes")
	@PostMapping
	public ResponseEntity createAssociado(@RequestBody @Valid AssociadoDto associadoDto) {
		
		try {
				associadoService.save(associadoDto);
				return new ResponseEntity<>(HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(new ResponseMsg(
                    HttpStatus.NOT_ACCEPTABLE,
                    "Cadastro de associado não permitido!", e.getMessage()), HttpStatus.NOT_ACCEPTABLE);

		}
	}
	
	@SuppressWarnings("rawtypes")
	@PutMapping
	public ResponseEntity atualizar(@RequestBody @Valid Associado associado) {
		repository.save(associado);
	
		return new ResponseEntity<>(HttpStatus.OK);
		
		
	}
	
}
