package br.com.votacao.model.dto;

import java.util.Date;

import br.com.votacao.model.enums.TipoSituacaoPautaEnum;
import lombok.Data;

@Data
public class PautaDto {
   private long id;
   private String titulo;
   private String descricao;
   private Date dataCriacao = new Date();
   private TipoSituacaoPautaEnum situacao;
   private long totalVostos;
   private long totalVostosSim;
   private long totalVostosNao;
   private long totalAbstencao;
  
}
