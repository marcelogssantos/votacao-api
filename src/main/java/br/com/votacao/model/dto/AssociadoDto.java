package br.com.votacao.model.dto;

import lombok.Data;

@Data
public class AssociadoDto {

    private String cpf;
    private String nome;
    private boolean ativo;
    
}